# get-browser-binary

Install specific browser versions for Chromium, Firefox and Edge, and their
matching [selenium webdriver](https://www.selenium.dev/selenium/docs/api/javascript/index.html).

## Getting started

The sample below shows how to install the latest Chromium and run it using
selenium webdriver:

```javascript
import {BROWSERS} from "@eyeo/get-browser-binary";

(async function example() {
  let {binary} = await BROWSERS.chromium.installBrowser("latest");
  console.log(`Chromium executable location: ${binary}`);

  let driver = await BROWSERS.chromium.getDriver("latest");
  await driver.navigate().to("https://example.com/");
  await driver.quit();
})();
```

[test/browsers.js](https://gitlab.com/eyeo/developer-experience/get-browser-binary/-/blob/main/test/browsers.js)
provides other usage examples of the library.

For more information, please refer to the [API documention](https://gitlab.com/eyeo/developer-experience/get-browser-binary/-/jobs/artifacts/main/file/docs/index.html?job=docs).
If you are already on the documentation page, you may find the API contents on
the right side.

### Supported browser versions

- Chromium >= 77 (Chromium ARM >= 92)
- Firefox >= 68
- Edge >= 114

Note: Installing Edge is not supported on Windows. It is assumed to be installed
because it is the default browser on that platform. On macOS, only the latest
Edge version is supported.

### Verbose logging

Set the `VERBOSE` environment variable to `"true"` to get verbose logging on
download requests.

### Offline execution

It is possible to run browsers offline as long as they have been previously
installed. Example:

```javascript
// Online
let {binary} = await BROWSERS[browser].installBrowser(version);
console.log(binary); // keep the browser binary location to use it offline
let driver = await BROWSERS[browser].getDriver(version); // let the driver binary download

// Offline
let customBrowserBinary = "<browser binary location>";
let driver = await BROWSERS[browser].getDriver(version, {customBrowserBinary});
```

## Development

### Prerequisites

- Node >= 18
- npm >= 9

### Installing/Updating dependencies

```shell
npm install
```

### Folders to ignore / cache

All browser and webdriver files will be extracted to the `./browser-snapshots`
folder, which probably makes sense to be ignored (for instance, by adding it to
`.gitignore`).

On the other hand, `./browser-snapshots/<browser_name>/cache` will hold all the
downloaded installation files required by `<browser_name>`. Therefore, it may be
useful to add `./browser-snapshots/*/cache` to the list of cached folders in
your CI pipeline configuration.

## Testing

Running all tests:

```shell
npm test
```

### Options

The `grep` option filters the tests to run with a regular expression. Example:

```shell
npm test -- --grep "chromium.*latest"
```

The `timeout` option overrides the timeout defined by `.mocharc.json`.
Increasing the timeout may be useful on slow connection environments:

```shell
npm test -- --timeout <ms>
```

By default, tests delete the `./browser-snapshots` before each `Browser` suite
runs. To change that behavior you may set the `TEST_KEEP_SNAPSHOTS` environment
variable to `true`. Example:

```shell
TEST_KEEP_SNAPSHOTS=true npm test
```

### Test server

Tests use a local http server, which is managed by the `npm test` command. If
needed, the test server can also run independently:

```shell
npm run test-server
```

Then tests may be executed on a separate session. Example:

```shell
npm run test-suite -- --grep "chromium.*latest"
```

### Running tests on Docker

Useful to reproduce the CI environment of the `test:browsers:linux` job.

#### Intel/AMD architecture

```shell
docker build -f test/docker/Dockerfile -t browsers .
docker run --shm-size=512m -it browsers
```

The `grep` and `timeout` options can also be used on Docker via the `TEST_ARGS`
parameter:

```shell
docker run --shm-size=512m -e TEST_ARGS="--grep chromium.*latest --timeout 100000" -it browsers
```

#### ARM architecture (M1/M2 Apple Silicon)

The run is done emulating the AMD architecture. Requirements:

- macOS >= 13 (Ventura)
- Rosetta
- The feature "Use Rosetta for x86/amd64 emulation on Apple Silicon" enabled in Docker

The `--platform` option should be used when running the image:

```shell
docker run --platform linux/amd64 --shm-size=512m -e TEST_ARGS="--grep chromium.*latest" -it browsers
```

## Building the documentation

```shell
npm run docs
```

## Code of Conduct

All contributors to this project are required to read and follow our
[code of conduct](./CODE_OF_CONDUCT.md).
