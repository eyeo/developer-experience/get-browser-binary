# Unreleased

# 0.20.2

- Fixes getting majorVersion on Edge (!133)

# 0.20.1

- Fixes enabling developer mode (!131)

# 0.20.0

- Enable developer mode for incognito tests (!126)
- Adds support to beta and dev channels for Edge in macOS (!127)
- Enable developer mode for latest versions of chromium browsers (!128)

# 0.19.0

- Fixes an issue starting with Firefox version 135 which prevented the binary to be downloaded (#82)
- `getInstalledVersion` now returns only the version number (#81)

## Testing

- Test extensions internal pages can now load normally. The development version of Chromium 133 got the `driver.getAllWindowHandles()` issue fixed, therefore the workaround is no longer needed. (!121)

# 0.18.0

- Adds support for Firefox dev channel (!116)
- Drops support for old Edge versions (#79)
- Fixes `enableExtensionInIncognito()` to keep working with Chromium +131 (!117)

## Testing

- Changes the way test extensions load internal pages. With Chromium +133,
`driver.getAllWindowHandles()` can't see tabs opened by an extension if the url
option opens an extension page. Please check the example provided by !119 in
order to workaround that issue.

# 0.17.0

- Stops calling `driver.close()` in `enableExtensionInIncognito()`
on non-Windows platforms (!114)
- Add instructions for offline execution (!110)
- Adds option to setup proxy through PAC file for Firefox (!115)

# 0.16.0

- Starts using selenium's automated driver management. That means additional
`chromedriver` and `msedgedriver` packages are no longer needed (#67)
- Loads the extension in Firefox by local path instead of being copied to a
temporary folder (!104)
- Checks if extensions can be loaded beforehand, by ensuring they contain a
`manifest.json` file. If not, a new `Extension manifest file not found` error
will be thrown (#71)
- Refactors headless runs by not using webdriver's deprecated headless() method
(#72)

### Testing

- Fixes an issue with `npm test` which would fail when the `grep` option would
include parenthesis (#69)
- Adds the browser version information in the "runs" test (#74)

### Notes for integrators

- Please make sure that your selenium-webdriver package version is at least
4.15.0 (the one used in this release). Also make sure that you are not using the
`chromedriver` nor `msedgedriver` packages, otherwise the automated driver
management may not work as expected (#67)
- If you experience chromedriver issues, try deleting the `/browser-snapshots`
cache folder.

# 0.15.0

- Fixes the downloads of Chromium versions < 91 by replacing the remaining
usages of omahaproxy API (now removed) with chromiumdash (#55)
- Updates to selenium webdriver 4.15.0 (#66)

### Testing

- Uses AMD emulation on ARM docker test runs (!93)

# 0.14.0

- Increases minimum supported browser versions (#64)
- Sets a minimum supported version for Chromium ARM (#54)
- Upgrades minimum node version to node 18 (#63)
- Fixes an occasional issue where the latest Edge on Windows could not find its
version on Chromium Dash (#61)
- Enables `dom.promise_rejection_events.enabled` for Firefox 68 (#49)
- Adds optional verbose logging on download requests (!79)
- Refactors `src/browsers.js` file (#65)

### Testing

- Adds a local http server for tests (#50)
- Allows running Firefox ARM in the Docker ARM image (!84)
- Skip other browser tests when install fails (#58)
- Fixes an issue where Linux Edge tests would fail when not following a very
specific order (#57)
- Reduces the size of the Windows CI log (#62)

### Notes for integrators

- The minimum broser versions are now Chromium >= 77 (Chromium ARM >= 92) and
Firefox >= 68. Edge keeps the same version (>= 95)
- The minimum required node version is now 18

# 0.13.0

- Fixes an issue that prevented the webdriver from locating the Edge binary on
Linux (#60)
- Uses headless new mode on Edge >= 116 (!74)
- Replaces omahaproxy API (deprecated) with chromiumdash (!73)
- Throws a `browser not installed` error when `customBinaryPath` doesn't exist
(!72)

### Testing

- Changes the test extension in incognito mode from spanning to split (#59)
- Runs Linux Edge tests in a different browser version order (!71)

# 0.12.0

- Adds a new field `customBrowserBinary` to `getDriver()` options parameter that
accepts the path of a custom (pre-installed) browser binary. When used, that is
the browser that will run (#56)
- Fixes Edge install URLs for mac. Unfortunately, the new URLs provided by Edge
offer a limited number of old versions only. For that reason `latest`
becomes the only accepted value for Edge on mac (!64). Please see #56 as a
possible workaround
- Fixes an issue with Chromium 115 that prevented loading extensions in
incognito mode (#52)
- Logs URL info on previously uncaught http errors (!67)

### Testing

- Fixes recent failures when running Edge tests (#48)

# 0.11.0

- Fixes an issue on arm64 edgedriver links that prevented the driver to run on
M1/M2 machines (!62)

### Testing

- Enables running docker tests on arm64 platforms on Chromium (partially) and
Firefox (!61)
- Changes `TEST_URL` to a specific testpages url (!62)

# 0.10.0

- Added handling of the new headless mode in Chromium (#45)
- Fixed an issue that prevented Chromium to be installed on macOS ARM processors
(!59)
- Changed documentation nullable parameters to optional parameters (#41)
- Increased the npm geckodriver version to 3.1.0 (!57)
- Fixed an issue with Windows Edge and msedgedriver, which occasionally failed
when building the driver (!56)

# 0.9.0

- Fixed a Chromium install issue by increasing the value of
`MAX_VERSION_DECREMENTS` (!52)
- `installBrowser()`, `getDriver()` and `download()` have a new optional timeout
parameter on file downloads (!51)

### Testing

- Added a test checking that downloaded browser-snapshot files are actually
cached (#35)
- Tests running the minimum firefox version showed occasional failures. That was
fixed by increasing the shared memory size of the docker image (!50)

# 0.8.0

- Move `takeFullPageScreenshot` to the utils module (#38)
- Add keywords to package.json (#36)

### Testing

- Log the running browser version on test suites (#37)

### Notes for integrators

- `takeFullPageScreenshot()` has been moved from the browsers module to the
utils module (!46).

# 0.7.0

- Implement takeFullPageScreenshot (#32)
- Unify error messages (#22)
- Document when a browser binary is downloaded or installed (#30)
- Use cached driver binary if it already exists (#20)
- Remove chromedriver dependency (#33)
- Fallback mechanism on minor browser versions (Edge) (#27)

### Testing

- Use a manifest V3 extension on latest chromium-based browsers tests (#31)
- Allow test run to not delete browser snapshots (#28)
- Add a timeout option to run the tests (#24)

### Notes for integrators

- The `downloadBinary` function has been renamed to `installBrowser` (!38).

# 0.6.0

- Removes Opera support (#26)
- Improves error handling when the downloaded browser version doesn't match
known channels (#25)
- Fixes an Edge running issue on Windows (32-bit) (#23)

# 0.5.0

- Enables Edge binary downloads on MacOS (!25)
- Sets unsupported browser versions (#16)
- Fixes the Edge repo url (!27)
- Fixes an issue when downloading Opera (#21)
- Fixes redundant Opera installations on Linux (!24)

# 0.4.0

- Adds Opera as a supported browser (#7)
- Enables Edge binary downloads on Linux (!18)
- Adds a code of conduct (#15)

# 0.3.0

- Adds incognito mode support to all browsers (#5)
- Exports the utils download function (#12)
- Adds the details of the directories to ignore / cache to the README (#13)
- Fixes Chromium tests execution on all platforms (#3)
- Updates the geckodriver package (#14)

# 0.2.0

Change `Browser.getDriver` signature to use `driverOptions` parameter.

# 0.1.1

Change project name to `get-browser-binary`.

# 0.1.0

Initial release.
