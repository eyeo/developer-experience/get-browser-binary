
function FindProxyForURL(url, host) {
  if (host === "testpages.adblockplus.org")
    return "PROXY http://localhost:3000";

  return "DIRECT";
}
