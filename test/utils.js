/*
 * Copyright (c) 2006-present eyeo GmbH
 *
 * This module is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import fs from "fs";
import {expect} from "expect";
import path from "path";

import {snapshotsBaseDir, download} from "../index.js";
import {TEST_SERVER_URL} from "./test-server.js";

describe("Utils", () => {
  it("defines a browser snapshots folder", () => expect(snapshotsBaseDir)
    .toBe(path.join(process.cwd(), "browser-snapshots")));

  let resourceUrl = `${TEST_SERVER_URL}/download-test.txt`;
  let destFile = path.join(snapshotsBaseDir, "download-test.txt");
  const expectedFileContents = "Download test";

  it("downloads resources", async() => {
    await fs.promises.rm(destFile, {force: true});
    await download(resourceUrl, destFile);
    let contents = await fs.promises.readFile(destFile, {encoding: "utf8"});
    expect(contents).toEqual(expect.stringContaining(expectedFileContents));
  });

  it("downloads resources on long timeout", async() => {
    await fs.promises.rm(destFile, {force: true});
    await download(resourceUrl, destFile, 10000);
    let contents = await fs.promises.readFile(destFile, {encoding: "utf8"});
    expect(contents).toEqual(expect.stringContaining(expectedFileContents));
  });

  it("does not download on short timeout", async() => {
    try {
      await download(resourceUrl, destFile, 1);
      throw new Error("Download timeout didn't throw");
    }
    catch (err) {
      expect(err).toBe("Download timeout after 1ms");
    }
  });

  it("does not download invalid resources", () =>
    expect(download("invalid", destFile)).rejects.toThrow("Invalid URL"));
});
